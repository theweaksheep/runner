# How to install ? #
``` bash
npm install --save https://qphilippot@bitbucket.org/qphilippot/runner.git
```

# Usage #

## Runner ##

An instance represent a periodic task.

### Create a Task ###
``` javascript 
const Runner = require('runner').Runner; // import module
const ScheduledTask = new Runner({
    phase: 0, // no phase
    name: 'my task', 
    peroid: 1000, // every seconds, this field is mandatory
    sensivity: 0, // if interval is too short, sensivity allow to avoid to perform a task
});
```

### Sensivity ###

Timers are not accurencies. If for some reason you got a draft, you could prefer wait for next period to perform your task. Sensivity defined how your task will deal with draft. 0 is minimum, it will ignore draft. 1 is max, if your timer is late (1 ms), your task is aborted for this period.

## RunnerManager ##

Store periodics tasks in one manager to improve your control.

### Create a Manager ###
``` javascript 
const Runner = require('runner'); // import module
const ScheduledTask = require('...'); // assume you already defined a class of Runner

const ScheduledTaskManager = new Runner.RunnerManager({
    type: ScheduledTask 
});
```