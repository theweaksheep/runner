const assert = require('chai').assert;
const Runner = require('../../src/Runner.model');

describe('Runner class unit', () => {
    let instance = null
    const expected_name = 'periodic-task';

    before(() => {
        instance = new Runner({
            period: 1000,
            name: expected_name
        });
    })

    it('Can create a "Running" instance', () => {    
        assert.exists(instance);
        assert.strictEqual(instance.name, expected_name); 
    })

    it('Have a timerID after start call', () => {
        instance.start();
        assert.exists(instance.timerID);
    })

    it('Clear timerID after stop call', () => {
        instance.stop();
        assert.isNull(instance.timerID);
    })

    after(() => {
        if (typeof instance !== 'undefined') {
            instance.stop();
        } 
    }) 
});
	