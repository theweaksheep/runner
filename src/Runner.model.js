const minDelta = 200;

module.exports = class Runner {
    constructor(settings = {}) { // adding a blank object so it can be defined 
		if (typeof settings.period === 'undefined' || isNaN(settings.period)) {
			throw new Error('Runner must have a period');
		}

		this.phase = settings.phase || 0;
		this.name = settings.name;
		this.period = parseInt(settings.period, 10); 
		this.sensivity = settings.sensivity || 0; // 1 misisipi, 2 misisipi = (the project of threading - simulaitons) (monitor the prices in detail)

		this.status = 'off'; // check 1/0 // default 0
		this.timerID = null; 
	}
	
	/** check if the periodic task is running and stop it */
	dedupe_start() {
		if (this.status === 'on') {
			if (this.timerID === null) {
				// incoherence detection
				throw new Error('Incoherence detected with Runner');
			}

			// prevent multiple interval running by killing current if is set
			this.stop();
		}
	}

	/** compute most accuracy timeout for next execution according to period */
	prepareNextTick() {
		const currentTimeIndicator = Math.floor((Date.now() + this.phase) / this.period); // checking of time 
		let nextTickDate = ((currentTimeIndicator + 1) * this.period) + this.phase; // checks on the next time after currentTimeIndicator

		// may i wait for timeIndicator + 2 ?
		const delta = nextTickDate - Date.now(); 
		if (delta < minDelta ||  delta < this.sensivity * this.period) {
			nextTickDate = ((currentTimeIndicator + 2) * this.period) + this.phase;
		}

		this.timerID = setTimeout(async () => {
			if (this.status === 'on') {
				await this.run();
				this.prepareNextTick();
			}
		},  nextTickDate - Date.now());
		
	}

	/** start a new periodic task */
	start() {
		this.dedupe_start();
		this.timerID = 'not null'; // placeholder to deal with concurrency access
		this.status = 'on';
		setTimeout(() => {
			// remove lock after 200 ms 
			if (this.timerID === 'not null') {
				this.timerID = null;
			}
		}, 200);
		this.prepareNextTick();
	}

	/** stop a periodic task */
	stop() {
		this.status = 'off';
		clearTimeout(this.timerID);
		this.timerID = null;
	}

	updatePeriod(period) {
		this.period = period;

		if (this.status === 'on') {
			this.restart();
		}
	}

	restart() {
		this.stop();
		this.start();	
	}

	/** 
	 * Here is the code of your task.
	 * Override this function
	 * */
	async run() {
		console.log('[Runner] non-overrided run is called');
		return true;
	}

}