module.exports = class RunnerManager {
    constructor(settings) {
        this.type = settings.type;
        this.runners = {};
    }

    exists(runnerName) {
        return typeof this.runners[runnerName] !== 'undefined';
    }

    delete(runnerName) {
        const runner = this.runners[runnerName]; 
		if (typeof runner !== 'undefined') {
			runner.stop();
            delete this.runners[runnerName];
        }
    }

    createRunnerInstance(settings) {
        return new (this.type)(settings);
    }

    create(settings) {
        if (this.exists(settings.name)) {
            throw new Error(`Hey dude, this runner already exists ! (${settings.name}`);  
        }

        const runner = this.createRunnerInstance(settings);
        this.runners[settings.name] = runner;
        return runner;
    }

    getRunner(name) {
        return this.runners[name];
    }

    getRunnersKey() {
        return Object.keys(this.runners);
    }

    getRunners() {
        return this.runners;
    }
}